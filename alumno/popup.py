# -*- coding: utf-8 -*-
from osv import fields, osv
from datetime import date
class popup(osv.osv):
    _name = "popup"
    _description = "Popup"

    def procesar(self, cr, uid, ids, context= None):
        print "Escribe tu código aquí"
        print "ACTIVE_MODEL:", context["active_model"]
        print "ACTIVE_IDS:", context["active_ids"] #imprime ids de los alumnos seleccionanos en el filtro
        return True

    _columns = {
        'fecha_reg' : fields.date('Fecha de registro', readonly=True),
    }
    
    _defaults = {
        "fecha_reg": str(date.today()),
    }

popup()
