# -*- coding: utf-8 -*-

import time

from report import report_sxw

class report_alumno(report_sxw.rml_parse):

    def __init__(self, cr, uid, name, context):
        super(report_alumno, self).__init__(cr, uid, name, context=context)
        self.localcontext.update({
            'time': time,
            'alumno_details': self.alumno_details,
        })
        self.cr = cr
        self.uid = uid
        self.context = context

    def alumno_details(self):
        #se dejo los print para que vean que valores tiene cada
        #una de las variables
        result = []
        cr = self.cr
        uid = self.uid
        context = self.context
        print "=" * 15
        print "uid", uid
        print "context", context
        print "=" * 15
        active_id = context['active_id']

        alumno_obj = self.pool.get('alumno').browse(cr, uid, active_id)

        cod_alumno = alumno_obj.cod_alumno
        nombres = alumno_obj.nombres
        apellido_paterno = alumno_obj.apellido_paterno
        apellido_materno = alumno_obj.apellido_materno
        foto = alumno_obj.foto
        dni = alumno_obj.dni
        fecha_nacimiento = alumno_obj.fecha_nacimiento
        nacionalidad = alumno_obj.nacionalidad.name
        departamento = alumno_obj.departamento_id.name
        direccion = alumno_obj.direccion
        tel_cel = alumno_obj.tel_cel
        observaciones = alumno_obj.observaciones

        result.append(
                {
                "cod_alumno": cod_alumno,
                "nombres": nombres,
                "apellido_paterno": apellido_paterno,
                "apellido_materno": apellido_materno,

                "foto": foto,
                "dni": dni,
                "fecha_nacimiento": fecha_nacimiento,
                "nacionalidad": nacionalidad,
                "departamento": departamento,
                "direccion": direccion,
                "tel_cel": tel_cel,
                "observaciones": observaciones,
                }
            )

        return result

#report.ReportAlumno (El nombre del reporte que definimos en el xml)
#alumno (el modulo)
#addons/alumno/report/alumno.rml (dirección del rml)
#parser=report_alumno (parseamos)
#header=False (Si le ponemos True asignara la cabecera y el pie de página del openerp por defecto)

report_sxw.report_sxw('report.ReportAlumno', 'alumno', 'addons/alumno/report/alumno.rml', parser=report_alumno, header=False)

