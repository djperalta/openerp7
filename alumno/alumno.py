# -*- coding: utf-8 -*-
from osv import fields, osv
from openerp import pooler, tools

class alumno(osv.osv):
    _name = "alumno"
    _description = "Registro alumno"
    _order = "id desc"
    
    def accion_planificada(self, cr, uid, param):
        print "=" * 20
        print "Estamos en accion_planificada"
        print "=" * 20
        print "Aquí toda la lógica de la programación."

        print "param", param

        return True

    def call_form(self, cr, uid, ids, context=None):
        print "Estamos en el metodo: call_form"
        #========================================================================================
        #invocamos la metodo ir.model.data que es donde se guardar los ids de las vistas
        #llamamos al metodo get_object_reference y le pasamos como parametro el nombre del modulo
        #y el id con que fue creado (<record id="view_users_simple_form" model="ir.ui.view">)
        #y asi podemos llamar el formulario que queramos con solo identificar su id.
        #========================================================================================
        dummy, view_id = self.pool.get('ir.model.data').get_object_reference(cr, uid, 'base', 'view_users_simple_form')

        return {
        'name': "Formulario Usuarios llamado desde Alumno", #titulo del formulario
        'view_mode': 'form', #modo de la vista
        'view_id': view_id,
        'view_type': 'form',
        'res_model': 'res.users', #modelo origen
        'type': 'ir.actions.act_window',
        'nodestroy': True,
        'target': 'new', #para que se muestre el formulario en la misma ventana
        'domain': '[]',
        'context': {}
        }

    def _get_image(self, cr, uid, ids, name, args, context=None):
        result = dict.fromkeys(ids, False)
        for obj in self.browse(cr, uid, ids, context=context):
            result[obj.id] = tools.image_get_resized_images(obj.foto, avoid_resize_medium=True)
        return result

    def _set_image(self, cr, uid, id, name, value, args, context=None):
        return self.write(cr, uid, [id], {'foto': tools.image_resize_image_big(value)}, context=context)

    _columns = {
        'nombres' : fields.char('Nombres', size=64, required=True),
        'apellido_paterno' : fields.char('Apellido Paterno', size=64),
        'apellido_materno' : fields.char('Apellidos Materno', size=64),
        'activo' : fields.boolean('Activo'),
        'direccion' : fields.char('Dirección', size=100),
        'tel_cel' : fields.char('Tel./Cel.', size=100),
        'foto' : fields.binary('Foto'),
        'dni' : fields.char('D.N.I.', size=8),
        'fecha_nacimiento' : fields.date('Fecha de Nacimiento'),
        'nacionalidad' : fields.many2one('res.country', 'Nacionalidad'),
        'observaciones' : fields.text('Observaciones'),
        #agregar el nuevo atributo
        'departamento_id' : fields.many2one('departamento', 'Departamento'),
        'attachment_ids' : fields.many2many('ir.attachment', 'alumno_attachment', 'alumno_id', 'attachment_id', 'Adjuntar Archivo'), #attachment
        'cod_alumno' : fields.char('Cod. Alumno', size=20),
        'user_id': fields.many2one('res.users', 'Owner', help="Owner of the note stage.", required=True, ondelete='cascade'),
        
        'image_medium': fields.function(_get_image, fnct_inv=_set_image,
            string="Medium-sized image", type="binary", multi="_get_image",
            store={
                'alumno': (lambda self, cr, uid, ids, c={}: ids, ['foto'], 10),
            },
            help="Medium-sized image of the product. It is automatically "\
                 "resized as a 128x128px image, with aspect ratio preserved, "\
                 "only when the image exceeds one of those sizes. Use this field in form views or some kanban views."),
        'image_small': fields.function(_get_image, fnct_inv=_set_image,
            string="Small-sized image", type="binary", multi="_get_image",
            store={
                'alumno': (lambda self, cr, uid, ids, c={}: ids, ['foto'], 10),
            },
            help="Small-sized image of the product. It is automatically "\
                 "resized as a 64x64px image, with aspect ratio preserved. "\
                 "Use this field anywhere a small image is required."),
    }

    _defaults = {  
        'cod_alumno': lambda x, cr, uid, c: x.pool.get('ir.sequence').get(cr, uid, 'code-alumno'), #se obtiene la secuencia por medio del código(code-alumno)
        'user_id': lambda self, cr, uid, ctx: uid,
    }

alumno()

class departamento(osv.osv):
    _name = "departamento"
    _description = "departamento"

    _columns = {
        'name' : fields.char('Nombre', size=64, required=True),
        'codigo' : fields.char('Código', size=8, required=True),
    }

departamento()
