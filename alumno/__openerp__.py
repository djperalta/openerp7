{
    "name" : "Alumno",
    "version" : "0.1",
    "author" : "yaroslab",
    "website" : "http://yaroslab.com/",
    "category" : "Generic Modules/Others",
    "depends" : ["base"],
    "description" : "Registro alumno",
    "init_xml" : [
        "alumno_view.xml",
        "departamento.xml",
        "popup.xml",
        'secuencias.xml',
        'report_alumno.xml', # nueva linea
    ],
    'data': [],
    "demo_xml" : [],
    "active": False,
    "installable": True
}
